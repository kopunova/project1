<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\modules\admin\models\Project;
use app\modules\admin\models\Task;
use app\modules\admin\models\TaskSearch;
use app\models\SignupForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                        [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
		
		$searchModel = new TaskSearch();
		$searchModel->statusParam = '1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$dataProvider->setSort([
            'defaultOrder' => [
				'deadline' => SORT_ASC
			]
        ]);
		
		$dataProvider->sort->attributes['fire'] = [
            'asc' => ['fire' => SORT_DESC],
            'desc' => ['fire' => SORT_ASC],
        ];
				
		return $this->render('status1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);		
		
    }
		
	public function actionStatus2() {
		
        $searchModel = new TaskSearch();
		$searchModel->statusParam = '2';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$dataProvider->setSort([
            'defaultOrder' => [
				'deadline' => SORT_ASC
			]
        ]);
		
		$dataProvider->sort->attributes['fire'] = [
            'asc' => ['fire' => SORT_DESC],
            'desc' => ['fire' => SORT_ASC],
        ];
				
		return $this->render('status2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		
    }
	
	public function actionUpdate($id) {
		
		$model = Task::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
			
			$model->status = '2';
			
			if ($model->save()) {
				return $this->redirect(['index']);
			}
        }

        return $this->render('update', [
                    'model' => $model,
        ]);

    }	
	
	public function actionView($id) {
		
        return $this->render('view', [
                    'model' => Task::findOne($id),
        ]);

    }	
		
    public function actionSignup() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        $this->layout = '//main-signup';

        return $this->render('signup', [
                    'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        $this->layout = '//main-login';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
