<?php

namespace app\modules\admin\models;

use Yii;

class TaskGroupCreation extends \yii\base\Model {

    public $project_id;
    public $project_TZBinet;
    public $keywords;

    public function attributeLabels() {
        return [
            'project_id' => 'Проект',
            'project_TZBinet' => 'Проект в TZBinet',
            'keywords' => 'Ключевики',
        ];
    }

    public function rules() {
        return [
                [['project_id', 'project_TZBinet', 'keywords'], 'required'],
                [['project_id'], 'integer'],
                [['project_TZBinet'], 'string', 'max' => 255],
        ];
    }

    public function CreateMany() {

        if (!$this->validate()) {
            return null;
        }

        // нужно разбить $keywords на разные ТЗ		
        $keywords = trim($this->keywords);

        // разбиваем все на отдельные строки
        $strings = explode(PHP_EOL, $keywords);

        foreach ($strings as &$string) {
            // пустая строка является разделителем между разными ТЗ
            if (empty(trim($string))) {
                $string = '--space--';
            }						
            // ! является разделителем между основными и доп. ключевиками
            if (trim($string) == '!') {
                $string = '--!--';
            }
        }

        // возвращаем все в $keywords, но с нашими правками		
        $keywords = implode(PHP_EOL, $strings);		

        // разбиваем на разные ТЗ
        $arr_tasks = explode('--space--' . PHP_EOL, $keywords);

        foreach ($arr_tasks as $arr_task) {
            if (empty(trim($arr_task)))
                continue;

            $task = new Task();
            $task->status = '1';
            $task->project_id = $this->project_id;
            $task->project_TZBinet = $this->project_TZBinet;

            // разбиваем на осн. и доп. ключевики 
            $keywords = explode('--!--', $arr_task);
            $task->main_keywords = isset($keywords[0]) ? trim($keywords[0]) : '';
            $task->additional_keywords = isset($keywords[1]) ? trim($keywords[1]) : '';

            $task->save();
        }

        return true;
    }

}
