<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Task;

/**
 * TaskSearch represents the model behind the search form of `app\modules\admin\models\Task`.
 */
class TaskSearch extends Task
{	
	public $statusParam;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'group_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],
			'sort' => [
				'defaultOrder' => [
					'date_add' => SORT_DESC
				]
			]
        ]);
		
		$dataProvider->sort->attributes['fire'] = [
            'asc' => ['fire' => SORT_DESC],
            'desc' => ['fire' => SORT_ASC],
        ];
		
        $this->load($params);
		
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->statusParam,
			'project_id' => $this->project_id,
			'group_id' => $this->group_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);	

        return $dataProvider;
    }
}














