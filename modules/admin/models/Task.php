<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property int $project
 * @property string $project_TZBinet
 * @property string $date_add
 * @property string $deadline
 * @property string $main_keywords
 * @property string $additional_keywords
 * @property string $task_text
 */
class Task extends \yii\db\ActiveRecord {
	
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['status', 'project_id', 'project_TZBinet', 'main_keywords'], 'required'],
                [['project_id', 'group_id'], 'integer'],
                [['date_add', 'group_id', 'import_key'], 'safe'],
				['fire', 'default', 'value' => 0],
                [['name', 'status', 'project_TZBinet'], 'string', 'max' => 255],
				[['main_keywords', 'additional_keywords'], 'string', 'max' => 20000],
                [['task_text'], 'string', 'max' => 1500],
				[['deadline'], 'filter', 'filter' => function($value) {
                    if ($value == '')
                        return NULL;
                    return date('Y-m-d', strtotime($value));
                }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'project_id' => 'Проект',
            'projectName' => 'Проект',
            'project_TZBinet' => 'Проект в TZBinet',
            'date_add' => 'Дата создания',
            'deadline' => 'Дедлайн',
            'main_keywords' => 'Основные ключевики',
            'additional_keywords' => 'Дополнительные ключевики',
            'task_text' => 'ТЗ для автора',
			'fire' => 'Горит',
			'keywords' => 'Ключевики',
			'group_id' => 'Группа',
			'groupName' => 'Группа',
			'import_key' => 'Частотность импортируемых ключей',
        ];
    }
	
	public function getKeywords() {		
	
        return (trim($this->main_keywords) . PHP_EOL . '!' . PHP_EOL . trim($this->additional_keywords));
		
    }
	
	public function getDateAddFormat() {		
	
        return date('d.m.Y H:i:s', strtotime($this->date_add));
		
    }
	
	public function getDeadlineFormat() {		
	
        return date('d.m.Y', strtotime($this->deadline));
		
    }

    public function getStatuses() {

        return array(
            '1' => 'Исходные ключевики',
            '2' => 'Готовое ТЗ',
            '3' => 'Использованные ТЗ'
        );
    }
	
	public function getImportKey() {

        return array(
            '1' => 'НЧ',
            '2' => 'СЧ',
            '3' => 'ВЧ'
        );
    }

    public function getStatusName() {

        if ($this->status == 1)
            return 'Исходные ключевики';
        if ($this->status == 2)
            return 'Готовое ТЗ';
        if ($this->status == 3)
            return 'Использованные ТЗ';
    }

    // project 	
    public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getProjectName() {
        if (isset($this->project))
            return $this->project->name;
        else
            return '';
    }

	// groupName 	
   /* public function getGroup() {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    public function getGroupName() {
        if (isset($this->group))
            return $this->group->name;
        else
            return '';
    }*/
	
    public function beforeSave($insert) {

        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            $this->date_add = date('Y-m-d H:i:s');
            $this->deadline = date('Y-m-d', strtotime("+3 days"));
        }

        // установим название
        $strings = explode(PHP_EOL, $this->main_keywords);
		
        $name = '';
        if (isset($strings[0]) && !is_numeric(trim($strings[0]))) {
            $name = trim($strings[0]);
        } elseif (isset($strings[1]) && !is_numeric(trim($strings[1]))) {
            $name = trim($strings[1]);
        }

        $this->name = $name;

        return true;
    }

}
