<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все ТЗ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'projectName',
            'project_TZBinet',
            'statusName',
                [
                'attribute' => 'main_keywords',
                'format' => 'ntext',
            ],
                [
                'attribute' => 'additional_keywords',
                'format' => 'ntext',
            ],
                [
                'attribute' => 'task_text',
                'format' => 'ntext',
            ],
                [
                'attribute' => 'date_add',
                'value' => function($data) {
                    return date('d.m.Y H:i:s', strtotime($data->date_add));
                }
            ],
                [
                'attribute' => 'deadline',
                'value' => function($data) {
                    return date('d.m.Y', strtotime($data->deadline));
                }
            ],
            'name',
			'group_id',
        ],
    ])
    ?>

</div>

<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string) "task-{$model->id}", // type and id
]);?>














