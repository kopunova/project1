<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Project;
use app\modules\admin\models\Group;
use kartik\select2\Select2;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">  

        <div class="row">

            <div class="col-md-4"> 

                <?php $projects = Project::find()->orderBy('name')->all(); ?>

                <?=
                $form->field($model, 'project_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::Map($projects, 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите проект...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-4"> 
                <?= $form->field($model, 'project_TZBinet')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4"> 
                <?= $form->field($model, 'status')->dropDownList($model->statuses); ?>	
            </div>
        </div>

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#panel1">Ключевики</a></li>
            <li><a data-toggle="tab" href="#panel2">ТЗ для автора</a></li>
        </ul>

        <br>

        <div class="tab-content">
            <div id="panel1" class="tab-pane fade in active">

                <?= $form->field($model, 'main_keywords')->textArea(['maxlength' => true, 'rows' => 10]) ?>

                <?= $form->field($model, 'additional_keywords')->textArea(['maxlength' => true, 'rows' => 10]) ?>

            </div>
            <div id="panel2" class="tab-pane fade">

                <?= $form->field($model, 'task_text')->textArea(['maxlength' => true, 'rows' => 20]) ?>

            </div>
        </div>   	
               
		<?= $form->field($model, 'group_id')->textInput() ?>
				
		<?= $form->field($model, 'import_key')->dropDownList($model->importKey, ['prompt' => 'Выберите значение...']); ?>	

        <div class="row">

            <div class="col-md-2"> 				
                <?=
                $form->field($model, 'date_add')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy H:i:s',
                    'options' => ['class' => 'form-control', 'disabled' => true],
                ])
                ?>	
            </div>
            <div class="col-md-2"> 
                <?=
                $form->field($model, 'deadline')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => ['class' => 'form-control'],
                ])
                ?>	
            </div>
            <div class="col-md-6"> 
                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true]) ?>
            </div>
			 <div class="col-md-2"> 
                <?= $form->field($model, 'fire')->dropDownList([0 => 'Нет', 1 => 'Да']); ?>	
            </div>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Записать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string) "task-{$model->id}", // type and id
]);?>






















