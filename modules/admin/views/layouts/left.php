<aside class="main-sidebar">

    <section class="sidebar">  
	
        <?= dmstr\widgets\Menu::widget( 
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Админ. панель', 'options' => ['class' => 'header']],
					['label' => 'Проекты', 'icon' => 'book', 'url' => ['project/index']],	
					['label' => 'ТЗ <span class="glyphicon glyphicon-menu-down" style="float:right;padding-right:15px;padding-top:5px;font-size:9px;"></span>', 'encode' => false, 'icon' => 'calendar', 'url' => ['task/index']],
					['label' => 'Исходные ключевики', 'icon' => 'ca lendar-o', 'url' => ['task/status1'],],
					['label' => 'Готовое ТЗ', 'icon' => 'c alendar-check-o', 'url' => ['task/status2'],],
                    ['label' => 'Использованные ТЗ', 'icon' => 'a rchive', 'url' => ['task/status3'],],			
					// fa fa-sort-desc
					/*['label' => 'ТЗ <span class="fa fa-sort-desc" style="float:right;padding-right:15px;"></span>', 'encode' => false, 'icon' => 'calendar', 'url' => ['task/index']],*/
					/*[
                        'label' => 'ТЗ',
                        'icon' => 'calendar',
                        'url' => 'task/index',
						'options' => ['class' => 'active'],
                        'items' => [
                            ['label' => 'Все ТЗ', 'icon' => 'calendar', 'url' => ['task/index'],],
							['label' => 'Исходн. список ключевиков', 'icon' => 'calendar-o', 'url' => ['task/status1'],],
							['label' => 'Готовое ТЗ', 'icon' => 'calendar-check-o', 'url' => ['task/status2'],],
                            ['label' => 'Готовое ТЗ (использовано)', 'icon' => 'archive', 'url' => ['task/status3'],],
                            
                        ],
                    ],*/     				
					//['label' => 'Группы ТЗ', 'icon' => 'folder', 'url' => ['group/index']],
					['label' => 'Пользователи', 'icon' => 'user-o', 'url' => ['user/index']],				
				],					
            ]			
			
        ) ?>

    </section>

</aside>

















