<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Task;
use app\modules\admin\models\TaskSearch;
use app\modules\admin\models\TaskGroupCreation;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Group;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
				
		return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStatus1() {
		
		$searchModel = new TaskSearch();
		$searchModel->statusParam = '1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
						
		return $this->render('status1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionStatus2() {

        $searchModel = new TaskSearch();
		$searchModel->statusParam = '2';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
				
		return $this->render('status2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		
    }

    public function actionStatus3() {

        $searchModel = new TaskSearch();
		$searchModel->statusParam = '3';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
				
		return $this->render('status3', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		
    }

    public function actionUsed() {

        $keylist = Yii::$app->request->post('keylist');

        foreach ($keylist as $key) {
            $task = Task::findOne($key);
            $task->status = '3';
            $task->save();
        }

        return 'Статусы обновлены.';
    }
	
	public function actionFire() {

        $keylist = Yii::$app->request->post('keylist');

        foreach ($keylist as $key) {
            $task = Task::findOne($key);
            $task->fire = 1;
            $task->save();
        }

        return 'Пометка установлена.';
    }
	
	public function actionGroup() {

        $keylist = Yii::$app->request->post('keylist');
		
		$group_id = (new \yii\db\Query())
					->select(['max(group_id)'])
					->from('task')
					->scalar();	

		$next_group_id = $group_id + 1;
       
		foreach ($keylist as $key) {
            $task = Task::findOne($key);
            $task->group_id = $next_group_id;
            $task->save();
        }

        return 'ТЗ объединены в группу.';
    }
		
	public function actionDeleteSelectedTasks() {

        $keylist = Yii::$app->request->post('keylist');

        foreach ($keylist as $key) {
			Task::findOne($key)->delete();
        }

        return 'ТЗ удалены.';
    }

    public function actionExportTask() {

		$keylist = Yii::$app->request->post('keylist');
		
		if (empty($keylist)) return 'Отметьте флажками необходимые ТЗ.'; 
        
		$tasks = (new \yii\db\Query())->select([
                    'main_keywords', 'task_text'])
                ->from('task')
				->where(['id' => $keylist])
				->all();
				
		$taskText = '';		
		foreach ($tasks as $task) {		
            //$taskText = $taskText . ($taskText == '' ? '' : '<br>---<br>') . $task['name'] . '<br>' . $task['task_text'];
			$taskText = $taskText . ($taskText == '' ? '' : PHP_EOL . PHP_EOL) . $task['main_keywords'] . PHP_EOL . '!' . PHP_EOL . $task['task_text'];
        }
				
        return $taskText;
    }
	
	public function actionSearch() {

        $search_text = Yii::$app->request->get('text');
		
		$tasks = (new \yii\db\Query())->select([
                    'id', 'name'])
                ->from('task')
				->where(['like', 'main_keywords', $search_text])
				->orWhere(['like', 'additional_keywords', $search_text])
				->orWhere(['like', 'task_text', $search_text])
				->orderBy('id')
				->all();
		
		return $this->render('search', [
                    'tasks' => $tasks,
        ]);
		
    }

    public function actionTest() {			
					

		
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionGroupCreation() {

        $model = new TaskGroupCreation();

        if ($model->load(Yii::$app->request->post()) && $model->createMany()) {
            return $this->redirect(['index']);
        }

        return $this->render('group-creation', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
