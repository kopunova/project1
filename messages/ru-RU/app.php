﻿<?php

return [
    'Comments' => 'Комментарии',
	'Text' => 'Текст',	
	'Add comment' => 'Добавить комментарий',
	'Post comment' => 'Добавить комментарий',
	'Cancel' => 'Отмена',
	'Reply' => 'Ответить',
	'Edit' => 'Изменить',
	'Delete' => 'Удалить',
	'Updated at {date-relative}' => 'Обновлено {date-relative}',
	'Comment was deleted.' => 'Комментарий был удален.',
];
