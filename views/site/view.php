<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\module\Comments;

$this->title = $model->name;
?>

<div class="task-form">

	<h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">  
	
        <div class="row">
		
            <div class="col-md-4"> 
				<?= Html::activeLabel($model, 'project_id') . ': ' . $model->projectName ?> <br>
	
				<?= Html::activeLabel($model, 'project_TZBinet') . ': ' . $model->project_TZBinet ?>                
            </div>
			
            <div class="col-md-4"> 
				<?= Html::activeLabel($model, 'date_add') . ': ' . $model->dateAddFormat ?> <br>
	
				<?= Html::activeLabel($model, 'deadline') . ': ' . $model->deadlineFormat ?>
            </div>
			
            <div class="col-md-4"> 
				<?= Html::activeLabel($model, 'status') . ': ' . $model->statusName ?>
            </div>
			
        </div>
		
        <br>					
			<?= Html::textArea('', $model->keywords, ['id' => 'keywords', 'rows' => 15, 'style'=>'width:100%', 'disabled' => true]) ?>
			
			<div class="form-group">		
			</div>
			
            <?= $form->field($model, 'task_text')->textArea(['maxlength' => true, 'rows' => 20, 'disabled' => true]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= Comments\widgets\CommentListWidget::widget([
    'entity' => (string) "task-{$model->id}", // type and id
]);?>

<script type="text/javascript">
	
	function setSelection() {
		
		var target = document.getElementById('keywords');
		var rng, sel;
		if (document.createRange) {
		  rng = document.createRange();
		  rng.selectNode(target)
		  sel = window.getSelection();
		  sel.removeAllRanges();
		  sel.addRange(rng);
		  //document.execCommand("Copy");
		} else {
		  var rng = document.body.createTextRange();
		  rng.moveToElementText(target);
		  rng.select();
		}
	}

</script>

















