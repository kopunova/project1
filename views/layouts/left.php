<aside class="main-sidebar">

    <section class="sidebar">     

        <?= dmstr\widgets\Menu::widget( 
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Главное меню', 'options' => ['class' => 'header']],					
					['label' => 'Исходные ключевики', 'icon' => 'calendar-o', 'url' => ['site/index']],
					['label' => 'Готовое ТЗ', 'icon' => 'calendar-check-o', 'url' => ['site/status2']],                    
                ],
            ]
        ) ?>

    </section>

</aside>
