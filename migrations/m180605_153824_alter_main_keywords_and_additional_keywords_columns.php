<?php

use yii\db\Migration;

/**
 * Class m180605_153824_alter_main_keywords_and_additional_keywords_columns
 */
class m180605_153824_alter_main_keywords_and_additional_keywords_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('task', 'main_keywords', 'text');
		$this->alterColumn('task', 'additional_keywords', 'text');		
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180605_153824_alter_main_keywords_and_additional_keywords_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180605_153824_alter_main_keywords_and_additional_keywords_columns cannot be reverted.\n";

        return false;
    }
    */
}
