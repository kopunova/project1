<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m180523_211929_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
			'status' => $this->string(),
			'project_id' => $this->integer(11),
			'project_TZBinet' => $this->string(),
			'date_add' => $this->datetime(),
			'deadline' => $this->date(),
			'main_keywords' => $this->string(),
			'additional_keywords' => $this->string(),
			'task_text' => $this->string(1500),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}


















