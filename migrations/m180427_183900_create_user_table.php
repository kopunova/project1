<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180427_183900_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'email' => $this->string()->notNull()->unique(),
			'username' => $this->string(),			
			'phone' => $this->string(),
			'password' => $this->string()->notNull(),
			'registration_date' => $this->date()->notNull(),
			'role' => $this->string(),	
			
        ]);
		
		$this->insert('user', [
			'email' => 'admin@admin',
			'username' => 'admin',			
			'password' => '21232f297a57a5a743894a0e4a801fc3',  
			'role' => 'admin',
			'registration_date' => '2018-01-01',			
        ]);
		
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
