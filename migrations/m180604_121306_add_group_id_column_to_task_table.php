<?php

use yii\db\Migration;

/**
 * Handles adding group_id to table `task`.
 */
class m180604_121306_add_group_id_column_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'group_id', $this->integer());
		
		$this->createIndex(
            'idx-task-group_id',
            'task',
            'group_id'
        );
		
        $this->addForeignKey(
            'fk-task-group_id',
            'task',
            'group_id',
            'group',
            'id',
            'SET NULL',
			'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() 
	{        		
		$this->dropForeignKey(
            'fk-task-group_id',
            'task'
        );
		
		$this->dropIndex(
            'idx-task-group_id',
            'task'
        );
		
		$this->dropColumn('task', 'group_id');
    }
}













